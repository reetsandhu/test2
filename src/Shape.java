
public class Shape {
 int side=4;
String color="blue";
 
 public Shape(int s,String c)
 {
	this.side= s;
	this.color=c;
	
}

public String getColor() {
	return color;
}

public void setColor(String color) {
	this.color = color;
}

public int getSide() {
	return side;
}

public void setSide(int side) {
	this.side = side;
}
}